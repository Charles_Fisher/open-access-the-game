﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    /// <summary>
    /// How fast the player moves
    /// </summary>
    [SerializeField]
    float speed;

    /// <summary>
    /// How fast the player moves
    /// </summary>
    /// <value></value>
    public float Speed
    {
        get
        {
            return speed;
        }
        set
        {
            speed = Mathf.Min(Mathf.Max(minSpeed, value), maxSpeed);
        }
    }

    [SerializeField]
    float minSpeed;

    public float MinSpeed
    {
        get
        {
            return minSpeed;
        }
    }

    [SerializeField]
    float maxSpeed;

    public float MaxSpeed
    {
        get
        {
            return maxSpeed;
        }
    }

    bool movingLeft = false;
    bool movingRight = false;
    bool movingUp = false;
    bool movingDown = false;

    [SerializeField]
    Vector2 minPos;

    [SerializeField]
    Vector2 maxPos;

    [SerializeField]
    Rigidbody2D playerbody;

    [SerializeField]
    Animator animator;

    [SerializeField]
    new Collider2D collider;

    public Collider2D Collider
    {
        get
        {
            return collider;
        }
    }

    int score = 0;

    /// <summary>
    /// Player's total score
    /// </summary>
    /// <value></value>
    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
        }
    }

    /// <summary>
    /// Speed the player has at the start of the game
    /// </summary>
    float startSpeed;

    /// <summary>
    /// Speed the player has at the start of the game
    /// </summary>
    /// <value></value>
    public float StartSpeed
    {
        get
        {
            return startSpeed;
        }
        set
        {
            startSpeed = value;
        }
    }

    /// <summary>
    /// Current combo level for the player
    /// </summary>
    int combo;

    /// <summary>
    /// Player's current combo level
    /// </summary>
    /// <value></value>
    public int Combo
    {
        get
        {
            return combo;
        }
        set
        {
            combo = value;
            //Debug.LogWarning("Player combo: " + combo);
        
        }
    }

    bool acceptingInput = true;

    bool isGameOver = false;

    /// <summary>
    /// True if the player is currently accepting input
    /// </summary>
    /// <value></value>
    public bool AcceptingInput
    {
        get
        {
            return acceptingInput;
        }
        set
        {
            acceptingInput = value;
        }
    }

    void Start()
    {
        startSpeed = speed;
    
    }

    void Update()
    {
        if (acceptingInput && !isGameOver)
        {
            MoveCharacter();
        }
    }

    void FixedUpdate()
    {
        if (acceptingInput && !isGameOver)
        {
            ChangeTransform();
        }
    }

    void SetBoolsLeft()
    {
        movingUp = false;
        movingDown = false;
        movingLeft = true;
        movingRight = false;
    }

    void SetBoolsRight()
    {
        movingUp = false;
        movingDown = false;
        movingLeft = false;
        movingRight = true;
    }

    void SetBoolsUp()
    {
        movingUp = true;
        movingDown = false;
        movingLeft = false;
        movingRight = false;
    }


    void SetBoolsDown()
    {
        movingUp = false;
        movingDown = true;
        movingLeft = false;
        movingRight = false;
    }


    /// <summary>
    /// Sets the movement booleans for the player
    /// </summary>
    void MoveCharacter()
    {
        //TODO: Determine if mouse or keyboard
        if (Input.GetButtonDown("Down"))
        {
            SetBoolsDown();
        }
        else if (Input.GetButtonDown("Up"))
        {
            SetBoolsUp();
        }
        else if (Input.GetButtonDown("Left"))
        {
            SetBoolsLeft();
        }
        else if (Input.GetButtonDown("Right"))
        {
            SetBoolsRight();
        }
        else
        {
            float x = Input.GetAxis("LeftRightJoy");
            float y = Input.GetAxis("UpDownJoy");

            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                y = 0;
            }
            else if (Mathf.Abs(y) > Mathf.Abs(x))
            {
                x = 0;
            }
            else
            {
                x = 0;
                y = 0;
            }

            if (y < 0)
            {
                SetBoolsDown();
            }
            else if (y > 0)
            {
                SetBoolsUp();
            }
            else if (x < 0)
            {
                SetBoolsLeft();
            }
            else if (x > 0)
            {
                SetBoolsRight();
            }
        }
    }

    /// <summary>
    /// True if the player is invincible
    /// </summary>
    /// <returns></returns>
    public bool IsInvincible()
    {
        return animator.GetBool("Invincible");
    }

    /// <summary>

    /// <summary>
    /// Moves the player based upon the movement booleans
    /// </summary>
    void ChangeTransform()
    {
        Vector2 newPos = gameObject.transform.position;

        float newX = newPos.x + (movingLeft ? -1.0f * speed : (movingRight ? speed : 0));
        newPos.x = newX;

        float newY = newPos.y + (movingDown ? -1.0f * speed : (movingUp ? speed : 0));
        newPos.y = newY;

        playerbody.MovePosition(newPos);
    }
}
